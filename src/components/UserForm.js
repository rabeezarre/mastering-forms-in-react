import React, { useState } from "react";

const INITIAL_FORM_STATE = {
  firstName: "",
  lastName: "",
  age: "",
  employed: false,
  favoriteColor: "",
  sauces: [],
  bestStooge: "",
  notes: "",
};



const UserForm = () => {
  const [formData, setFormData] = useState(INITIAL_FORM_STATE);

  const handleChange = (e) => {
    const { name, value, type, checked } = e.target;
    setFormData((prevFormData) => ({
      ...prevFormData,
      [name]: type === "checkbox" ? checked : value,
    }));
  };

  const disableReset = JSON.stringify(formData) === JSON.stringify(INITIAL_FORM_STATE);

  const disableSubmit = JSON.stringify(formData) === JSON.stringify(INITIAL_FORM_STATE);

  const handleSaucesChange = (event) => {
    const { value } = event.target;
    const newSauces = [...formData.sauces];
    if (newSauces.includes(value)) {
      newSauces.splice(newSauces.indexOf(value), 1);
    } else {
      newSauces.push(value);
    }

    setFormData({
      ...formData,
      sauces: newSauces,
    });
  };

  const handleReset = () => {
    setFormData(INITIAL_FORM_STATE);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const check = isFormValid();

    if (!check) {
      return;
    }

    alert(
      `First Name: ${formData.firstName}\nLast Name: ${
        formData.lastName
      }\nAge: ${formData.age}\nEmployed: ${
        formData.employed ? "Yes" : "No"
      }\nFavorite Color: ${
        formData.favoriteColor
      }\nSauces: ${formData.sauces.join(", ")}\nBest Stooge: ${
        formData.bestStooge
      }\nNotes: ${formData.notes}`
    );
    handleReset();
  };

  const isFormValid = () => {
    const nameRegex = /^[A-Za-z ]+$/;
    const ageRegex = /^[0-9]+$/;
    let isValid = true;

    if (formData.firstName && !nameRegex.test(formData.firstName)) {
      isValid = false;
      document.getElementById("first-name").classList.add("invalid");
    } else {
      document.getElementById("first-name").classList.remove("invalid");
    }
    if (formData.lastName && !nameRegex.test(formData.lastName)) {
      isValid = false;
      document.getElementById("last-name").classList.add("invalid");
    } else {
      document.getElementById("last-name").classList.remove("invalid");
    }
    if (formData.age && !ageRegex.test(formData.age)) {
      isValid = false;
      document.getElementById("age").classList.add("invalid");
    } else {
      document.getElementById("age").classList.remove("invalid");
    }
    if (formData.notes && formData.notes.length > 100) {
      isValid = false;
      document.getElementById("notes").classList.add("invalid");
    } else {
      document.getElementById("notes").classList.remove("invalid");
    }
    return isValid;
  };

  return (
    <form className="form-container">
      <div className="form-inside-container">
        <div className="input-container">
          <div className="label-container">
            <label htmlFor="first-name" className="form-label">
              First Name
            </label>
          </div>

          <input
            type="text"
            id="first-name"
            name="firstName"
            value={formData.firstName}
            onChange={handleChange}
            className="form-control first-name-input"
          />
        </div>

        <div className="input-container">
          <div className="label-container">
            <label htmlFor="last-name" className="form-label">
              Last Name
            </label>
          </div>

          <input
            type="text"
            id="last-name"
            name="lastName"
            value={formData.lastName}
            onChange={handleChange}
            className="form-control last-name-input"
          />
        </div>

        <div className="input-container">
          <div className="label-container">
            <label htmlFor="age" className="form-label">
              Age
            </label>
          </div>

          <input
            type="text"
            id="age"
            name="age"
            value={formData.age}
            onChange={handleChange}
            className="form-control age-input"
          />
        </div>

        <div className="input-container form-check">
          <div className="label-container">
            <label htmlFor="employed" className="form-check-label">
              Employed
            </label>
          </div>
          <input
            type="checkbox"
            id="employed"
            name="employed"
            checked={formData.employed}
            onChange={handleChange}
            className="form-check-input"
          />
        </div>

        <div className="input-container">
          <div className="label-container">
            <label htmlFor="favorite-color" className="form-label">
              Favorite Color
            </label>
          </div>

          <select
            id="favorite-color"
            name="favoriteColor"
            value={formData.favoriteColor}
            onChange={handleChange}
            className="form-control"
          >
            <option value="">-- Select a color --</option>
            <option value="red">Red</option>
            <option value="blue">Blue</option>
            <option value="green">Green</option>
          </select>
        </div>

        <div className="input-container">
          <div className="label-container">
            <label className="form-label">Sauces</label>
          </div>
          <div className="options-container">
            <div className="form-check">
              <input
                type="checkbox"
                id="sauce-1"
                name="sauces"
                value="ketchup"
                checked={formData.sauces.includes("ketchup")}
                onChange={handleSaucesChange}
                className="form-check-input"
              />
              <label htmlFor="sauce-1" className="form-check-label">
                Ketchup
              </label>
            </div>
            <div className="form-check">
              <input
                type="checkbox"
                id="sauce-2"
                name="sauces"
                value="mustard"
                checked={formData.sauces.includes("mustard")}
                onChange={handleSaucesChange}
                className="form-check-input"
              />
              <label htmlFor="sauce-2" className="form-check-label">
                Mustard
              </label>
            </div>
            <div className="form-check">
              <input
                type="checkbox"
                id="sauce-3"
                name="sauces"
                value="mayonnaise"
                checked={formData.sauces.includes("mayonnaise")}
                onChange={handleSaucesChange}
                className="form-check-input"
              />
              <label htmlFor="sauce-3" className="form-check-label">
                Mayonnaise
              </label>
            </div>
            <div className="form-check">
              <input
                type="checkbox"
                id="sauce-4"
                name="sauces"
                value="guacamole"
                checked={formData.sauces.includes("guacamole")}
                onChange={handleSaucesChange}
                className="form-check-input"
              />
              <label htmlFor="sauce-4" className="form-check-label">
                Guacamole
              </label>
            </div>
          </div>
        </div>

        <div className="input-container">
          <div className="label-container">
            <label className="form-label">Best Stooge</label>
          </div>

          <div className="options-container">
            <div className="form-check">
              <input
                type="radio"
                id="stooge-1"
                name="bestStooge"
                value="moe"
                checked={formData.bestStooge === "moe"}
                onChange={handleChange}
                className="form-check-input"
              />
              <label htmlFor="stooge-1" className="form-check-label">
                Moe
              </label>
            </div>
            <div className="form-check">
              <input
                type="radio"
                id="stooge-2"
                name="bestStooge"
                value="larry"
                checked={formData.bestStooge === "larry"}
                onChange={handleChange}
                className="form-check-input"
              />
              <label htmlFor="stooge-2" className="form-check-label">
                Larry
              </label>
            </div>
            <div className="form-check">
              <input
                type="radio"
                id="stooge-3"
                name="bestStooge"
                value="curly"
                checked={formData.bestStooge === "curly"}
                onChange={handleChange}
                className="form-check-input"
              />
              <label htmlFor="stooge-3" className="form-check-label">
                Curly
              </label>
            </div>
          </div>
        </div>

        <div className="input-container">
          <div className="label-container">
            <label htmlFor="notes" className="form-label">
              Notes
            </label>
          </div>
          <textarea
            id="notes"
            name="notes"
            value={formData.notes}
            onChange={handleChange}
            className="form-control note-input"
          />
        </div>

        <div className="buttons-wrapper">
          <button type="submit" className="btn" onClick={handleSubmit} disabled={disableSubmit}>
            Submit
          </button>

          <button className="btn" onClick={() => handleReset()} disabled={disableReset}>
            Reset
          </button>
        </div>

        <div className="form-state">
          <pre>{JSON.stringify(formData, null, 2)}</pre>
        </div>
      </div>
    </form>
  );
};

export default UserForm;
